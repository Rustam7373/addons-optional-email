<?php

use Tygh\Registry;

function fn_generate_uniq_email()
{
	//Get a unique guest id
	$id = fn_get_id_for_email();

	//Module configuration
	$addon_info = Registry::get('addons.optional_email');

	//Сollect a string
	$email = $addon_info['email_prefix'] .$id . $addon_info['email_suffix'];

	return $email;
}

function fn_get_id_for_email()
{
	if (isset($_SESSION['settings']['cu_id']['value'])) {
		$id = $_SESSION['settings']['cu_id']['value'];
	} else {
		$id = 0;
	}

	return $id;
}