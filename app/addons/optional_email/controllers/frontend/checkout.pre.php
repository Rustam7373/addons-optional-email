<?php

//If POST request.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	//If there is an array with data about the buyer and in the array an empty email.
	if (isset($REQUEST['user_data']['email']) && empty($REQUEST['user_data']['email'])) {

		//Add email.
		$REQUEST['user_data']['email'] = fn_generate_uniq_email();
	}
}